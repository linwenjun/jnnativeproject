const route = (state={page: 'Main'}, action)=> {
  switch(action.type) {
    case 'setNavigator':
      _navigator = action.navigator;
      return action.route;

    case 'pushToNavigator':
      return action.route;
    
    default:
      return state;
  }
};

export default route