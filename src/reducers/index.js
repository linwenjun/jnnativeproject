import { combineReducers } from 'redux';
import { experts } from './experts.js';
import route from './route';

const rootReducers = combineReducers({
  experts,
  route
});

export default rootReducers;
