import React, {Component} from 'react';
import {
    Navigator, 
    View, 
    Text, Alert, ScrollView, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

var Orientation = require('react-native-orientation');

import Main from './components/Main';
import NullComponent from './components/NullComponent';
import ExpertDetail from './components/ExpertDetail';
import ExpertList from './components/ExpertList';
import Category from './components/Category';
import ServiceDetail from './components/ServiceDetail';
import ActivityDetail from  './components/ActivityDetail';
import PreviewDetail from './components/PreviewDetail';
import Discovery from './components/Discovery';
import MicroClass from './components/MicroClass';

const routeMap = {
  Main,
  ExpertDetail,
  ExpertList,
  Category,
  ServiceDetail,
  ActivityDetail,
  PreviewDetail,
  Discovery,
  MicroClass,
  NullComponent
};

class App extends Component {

  componentWillMount() {
    // Orientation.lockToPortrait();
  }

  render() {
    let Page = routeMap[this.props.route.page] || NullComponent;
    return (
        <View style={styles.app}>
          <Page />
        </View>
    )
  }
}

const styles = StyleSheet.create({
  app: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#FFF'
  },
  navigator: {
    flex: 1
  }
});

const mapStateToProps = state => state;

const wrapedApp = connect(mapStateToProps)(App);

export default wrapedApp;
