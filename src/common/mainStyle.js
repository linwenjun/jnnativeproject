import {StyleSheet} from 'react-native';

const mainStyle = StyleSheet.create({
  paragraph: {
    fontSize: 12,
    lineHeight: 19,
    color: '#666'
  }
});

export default mainStyle;