import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Alert,
Dimensions
} from 'react-native';

const wWidth = Dimensions.get('window').width;

import Menu from './Menu';
import MiddleMenu from './MiddleMenu';

const menuList = [{
  title: '主页',
  page: 'Main',
  icon: require('../../img/Main.png'),
  iconActivited: require('../../img/MainActivited.png')
}, {
  title: '分类',
  page: 'Category',
  icon: require('../../img/Rang.png'),
  iconActivited: require('../../img/RangActivited.png')
}, {
  title: '发现',
  page: 'Discovery',
  icon: require('../../img/Find.png'),
  iconActivited: require('../../img/FindActivited.png')
}, {
  title: '我',
  page: 'home',
  icon: require('../../img/User.png'),
  iconActivited: require('../../img/UserActivited.png')
}];

export default class NavMenus extends Component {
  render() {
    return (
        <View style={styles.menus}>
          <Menu style={styles.menu} {...menuList[0]}></Menu>
          <Menu style={styles.menu}  {...menuList[1]}></Menu>
          <MiddleMenu style={styles.menu}/>
          <Menu style={styles.menu}  {...menuList[2]}></Menu>
          <Menu style={styles.menu}  {...menuList[3]}></Menu>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  menus: {
    height: 50,
    overflow: 'visible',
    flexDirection: 'row'
  },
  menu: {
    width: wWidth / 5,
    alignItems: 'center'
  }
});