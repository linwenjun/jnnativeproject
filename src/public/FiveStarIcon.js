import React from 'react';
import {
  Image
} from 'react-native';

const FiveStarIcon = ({size=20, style})=> {
  return (
      <Image resizeMode="contain" style={[{
        height: size,
        width: size * 4
      }, style]} source={require('../../img/Star.png')}></Image>
  )
};

export default FiveStarIcon;
