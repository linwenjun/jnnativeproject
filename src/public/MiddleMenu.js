import React, {Component} from 'react';
import {View, StyleSheet, Image, Text} from 'react-native';

export default class MiddleMenu extends Component {
  render() {
    return (
        <View style={[styles.wrap, this.props.style]}>
          <Text style={styles.img}>?</Text>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  img: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#FFF'
  },
  wrap: {
    marginTop: -10,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#EC4F2F',
    alignItems: 'center',
    justifyContent: 'center'
  }
});