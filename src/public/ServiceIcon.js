import React from 'react';
import {
  Image
} from 'react-native';

defaultResource = require('../../img/advise_icon.png');

const iconMap = {
  advise: require('../../img/advise_icon.png'),
  training: require('../../img/training_icon.png'),
  consult: require('../../img/consult_icon.png'),
  talk: require('../../img/talk_icon.png')
};

const ServiceIcon = ({style, type, size=40})=> {
  return (
      <Image resizeMode="contain" style={[{
        width: size * 2 / 3,
        height: size
      }, style]} source={iconMap[type] || defaultResource}></Image>
  )
};

export default ServiceIcon;

