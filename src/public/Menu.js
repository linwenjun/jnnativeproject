import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableWithoutFeedback, Text, View, StyleSheet, Alert, Image } from 'react-native';

class Menu extends Component {
  constructor() {
    super();
    this.handlePress = this.handlePress.bind(this);
  }

  handlePress() {
    const {dispatch, page, route} = this.props;
    if(page === route.page) return;

    dispatch({
      type: 'pushToNavigator',
      route: {page}
    });

  }

  render() {
    const {iconActivited, icon, title, page, belong, route} = this.props;
    let isActivited = (belong || page === route.page);
    let ico = isActivited ? iconActivited : icon;
    let textStyle = isActivited ? {
      color: '#EC4F2F'
    } : {};
    return (
        <TouchableWithoutFeedback style={[styles.menu]} onPress={this.handlePress}>
          <View style={[this.props.style, styles.menuView]}>
            <Image style={styles.img}  source={ico}></Image>
            <Text style={[textStyle, styles.text]}>{title}</Text>
          </View>
        </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    alignItems: 'center'
  },
  menuView: {
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.3)'
  },
  img: {
    width: 27,
    height: 27,
    marginTop: 4,
    marginBottom: 4
  },
  text: {
    fontSize: 11
  }
});

const mapStateToProps= state => state;

const wrapedMenu = connect(mapStateToProps)(Menu);

export default wrapedMenu;