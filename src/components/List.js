import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TextInput } from 'react-native';

class List extends Component {
  render() {
    return (
        <View>
          {this.props.experts.map((expert, idx)=> (
              <Text key={idx}>{expert}</Text>
          ))}
          <TextInput
              style={{height: 40}}
              placeholder="Type here to translate!!"
          ></TextInput>
        </View>
    )
  }
}

const mapStateToProps= state => state;

const wrapedList = connect(mapStateToProps)(List);

export default wrapedList;