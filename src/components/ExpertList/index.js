import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image
} from 'react-native';

import DomainFilter from './DomainFilter';

import {
    ExpertItem
} from '../common';

const ExpertList = ()=> {
  return (
      <View style={styles.wrap}>
        <DomainFilter />
        <ExpertItem />
        <ExpertItem />
      </View>

  )
};

const styles = StyleSheet.create({
  wrap: {
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 30
  },
  expertItem: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  summary: {
    flex: 1,
    flexDirection: 'column'
  },
  nameWrap: {
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center'
  },
  name: {
    fontSize: 13,
    fontWeight: 'bold',
    marginRight: 8
  },
  title: {
    fontSize: 13,
    marginTop: 9
  },
  domain: {
    fontSize: 11,
    color: '#46B4E4',
    marginTop: 9
  },
  desc: {
    fontSize: 11,
    marginTop: 10
  }
});

export default ExpertList;