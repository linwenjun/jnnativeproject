import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const DomainFilter = ()=> {
  return (
      <View style={styles.wrap}>
        <View style={styles.btns}>
          <Text style={{
          color: '#F65153'
        }}>全部</Text>
          <Text>IT互联网</Text>
          <Text>制造业</Text>
          <Text>生活服务</Text>
          <Text>商务消费</Text>
        </View>
        <Icon style={{
          marginLeft: 10
        }} name="chevron-down" size={10} color="#F65153" />
      </View>

  )
};

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  btns: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  dropDown: {
    width: 24,
    height: 15,
    borderWidth: 1
  },
  dropDownIcon: {
    width: 24,
    height: 15
  }
});

export default DomainFilter;
