import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    ScrollView,
    Dimensions
} from 'react-native';

import {
    Space
} from '../../public';

import {Summary} from '../common';
import Intro from './Intro';
import ExpertList from './ExpertList';
import ApplyButton from './ApplyButton';

let wWidth = Dimensions.get('window').width;

const ActivityDetail = ()=> {
  return (
      <ScrollView style={styles.wrap}>
        <View style={styles.imageWrap}>
          <Image resizeMode="cover" style={styles.image} source={require('../../../img/activity_background.png')}/>
        </View>
        <Space />
        <Summary />
        <Space />
        <Intro />
        <Space />
        <ExpertList />
        <ApplyButton />
      </ScrollView>

  )
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: '#F8F8F8'
  },
  imageWrap: {
    flexDirection: 'column'
  },
  image: {
    width: wWidth,
    height: 290
  }
});

export default ActivityDetail;