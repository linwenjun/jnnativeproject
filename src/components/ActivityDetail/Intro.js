import React, {Compoent} from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import {
  GrayLine,
  Space
} from '../../public';

const Intro = ()=> {
  return (
      <View style={styles.wrap}>
        <Text style={styles.title}>活动介绍</Text>
        <Space />
        <GrayLine />
        <Space />
        <Text style={styles.content}>
          如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，者觉得在第一种情况基础……如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，者觉得在第一种情况基础……如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，者觉得在第一种情况基础……
        </Text>
        <Space />

        <View style={styles.info}>
          <Text style={styles.subTitle}>活动时间</Text>
          <Text style={styles.content}>12月12日</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.subTitle}>活动地点</Text>
          <Text style={styles.content}>北京</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.subTitle}>活动类型</Text>
          <Text style={styles.content}>线上活动</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.subTitle}>活动组织方</Text>
          <Text style={styles.content}>锦囊专家</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.subTitle}>参与人数</Text>
          <Text style={styles.content}>400人</Text>
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: '#FFF',
    padding: 15
  },
  title: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  content: {
    fontSize: 12,
    lineHeight: 18,
    color: 'rgba(0,0,0,0.6)'
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 6,
    marginBottom: 7
  },
  subTitle: {
    fontSize: 13,
    width: 76
  }

});

export default Intro;