import React, {Compoent} from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';

const ApplyButton = ()=> {
  return (
      <View style={styles.wrap}>
        <Text style={styles.text}>立即报名</Text>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: '#F65131',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 15,
    color: '#FFF'
  }
});

export default ApplyButton;