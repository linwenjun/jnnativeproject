import React, {Compoent} from 'react';
import {
  View,
  StyleSheet
} from 'react-native';

import ExpertItem from '../common/ExpertItem';

const ExpertList = ()=> {
  return (
      <View style={styles.wrap}>
        <ExpertItem />
        <ExpertItem />
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    padding: 10,
    backgroundColor: '#FFF'
  }
});

export default ExpertList;