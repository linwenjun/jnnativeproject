import React, {Component} from 'react';
import {View, Text, Image, TextInput, StyleSheet, Dimensions} from 'react-native';
import Swiper from 'react-native-swiper';


export default class MainCarousel extends Component {
  render() {
    return (
        <View style={styles.wrapper}>
          <Swiper autoplay={true} autoplayTimeout={5} height={230} showsPagination={false}>
            <View style={styles.slide1}>
              <Image style={styles.image}
                     source={require('../../../img/gallery.png')}></Image>
            </View>
            <View style={styles.slide1}>
              <Image style={styles.image}
                     source={require('../../../img/gallery.png')}></Image>
            </View>
            <View style={styles.slide1}>
              <Image style={styles.image}
                     source={require('../../../img/gallery.png')}></Image>
            </View>
          </Swiper>
        </View>

    )
  }
}

var styles = {
  wrapper: {

  },
  slide1: {
    marginTop: 20,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    height: 100,
    width: Dimensions.get('window').width - 20,
    flex: 1,
    borderRadius: 3
  }
};