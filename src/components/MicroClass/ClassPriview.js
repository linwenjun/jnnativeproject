import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ListView
} from 'react-native';

import {
    Space
} from '../common'

let data = ['a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c'];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let dataSource = ds.cloneWithRows(data);

export default class ClassPreview extends Component {

  constructor(props) {
    super(props);

    this.state = {
      pageText: '0/0'
    };

    this.changeRow = this.changeRow.bind(this);
  }

  changeRow(visibleRow) {
    let pages = Object.keys(visibleRow.s1).map(item => parseInt(item));
    let page = Math.max.apply(null, pages);
    this.setState({
      pageText: page + '/' + (data.length - 1)
    });
  }

  render() {
    return (
        <View>
          <View style={styles.title}>
            <Text style={styles.text}>课件预览</Text>
            <Text style={styles.page}>{this.state.pageText}</Text>
          </View>
          <Space />
          <ListView
              horizontal={true}
              dataSource={dataSource}
              onChangeVisibleRows={this.changeRow}
              renderRow={(rowData)=> {
              return (
                <Image resizeMode="cover" style={styles.ppt} source={require('../../../img/ppt.png')} />
              )
            }}
          />
        </View>
    )
  }

};

const styles = StyleSheet.create({
  wrap: {},
  title: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  page: {
    marginLeft: 5,
    fontSize: 12
  },
  ppt: {
    width: 300,
    height: 180,
    marginRight: 10
  }
});