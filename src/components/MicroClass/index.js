import React, {Compoent} from 'react';
import {
    View,
    ScrollView,
    StyleSheet
} from 'react-native';

import {
    SubTitle,
    Space,
    ActivityStatistics,
    ShrinkedActivityIntro,
    ExpertItem
} from '../common';

import ClassPriview from './ClassPriview';


const MicroClass = ()=> {
  return (
      <ScrollView style={styles.wrap}>
        <Space />

        <View style={styles.section}>
          <SubTitle>用游戏化改变世界</SubTitle>
          <ActivityStatistics />
        </View>

        <Space />

        <View style={styles.section}>
          <ShrinkedActivityIntro />
        </View>

        <Space />

        <View style={styles.section}>
          <ClassPriview />
        </View>

        <Space />

        <View style={styles.section}>
          <ExpertItem />
        </View>
      </ScrollView>
  )
};

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: '#F8F8F8',
    flex: 1
  },
  section: {
    padding: 15,
    backgroundColor: '#FFF'
  }
});

export default MicroClass;