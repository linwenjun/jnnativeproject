import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

const CategoryItem = ({title, subCategories})=> {

  while(!(subCategories.length % 3 === 0)) {
    subCategories.push({
      text: '锟斤拷',
      style: styles.empty
    })
  }
  return (
      <View style={styles.wrap}>
        <Text style={styles.cateName}>{title}</Text>
        <View style={styles.subCategory}>
          {
            subCategories.map(({text, style}, idx)=> (
                <Text key={idx} style={[styles.text, style || {}]}>{text}</Text>
            ))
          }
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'column'
  },
  cateName: {
    marginTop: 15,
    marginBottom: 10
  },
  subCategory: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  text: {
    width: 105,
    backgroundColor: '#F8F8F8',
    textAlign: 'center',
    marginBottom: 10,
    paddingTop: 7,
    paddingBottom: 7,
    color: '#333',
    fontSize: 12
  },
  empty: {
    backgroundColor: null,
    color: 'rgba(0,0,0,0)'
  }
});

export default CategoryItem;
