import React, {Component} from 'react';
import {
    View,
    StyleSheet
} from 'react-native';

import CategoryNavs from './CategoryNavs';
import CategoryList from './CategoryList';
import NavMenus from '../../public/NavMenus';

const categories = [{
  title: '互联网+',
  subCategories: [{
    text: '全部'
  }, {
    text: '互联网金融'
  }, {
    text: '在线教育'
  }, {
    text: '互联网金融'
  }]
}, {
  title: '互联网+',
  subCategories: [{
    text: '全部'
  }, {
    text: '互联网金融'
  }, {
    text: '在线教育'
  }, {
    text: '互联网金融'
  }]
}]

export default class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    }
  }

  componentDidMount() {
    setTimeout(()=> {
      this.setState({
        categories
      })
    })
  }

  render() {
    return (
        <View style={{
          flex: 1
        }}>
          <View style={{
            flex: 1
          }}>
            <CategoryNavs />
            <CategoryList
                categories={this.state.categories}
                style={styles.section}/>
          </View>
          <NavMenus />
        </View>
    )
  }
}

const styles = StyleSheet.create({
  section: {
    paddingLeft: 15,
    paddingRight: 15
  }
});