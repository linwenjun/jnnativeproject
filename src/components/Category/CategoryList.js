import React from 'react';
import {
    View
} from 'react-native';

import CategoryItem from './CategoryItem';

const CategoryList = ({style, categories})=> {

  return (
      <View style={style}>
        {
          categories.map((item, idx)=> (
              <CategoryItem key={idx}
                  {...item}
              />
          ))
        }
      </View>
  )
};

export default CategoryList;
