import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const yellow = '#F3523A';

const CategoryNavs = ()=> {
  return (
      <View style={styles.wrap}>
        <View style={styles.nav}>
          <View style={[styles.btn, {
            borderBottomColor: yellow
          }]}>
            <Text style={[styles.btnTxt, {
              color: yellow
            }]}>领域</Text>
          </View>
          <View style={styles.btn}>
            <Text style={styles.btnTxt}>行业</Text>
          </View>
        </View>
        <View style={styles.search}>
          <Icon style={{
            marginLeft: 10
          }} name="search" size={18} color={yellow}/>
        </View>
      </View>
  )
};


const styles = StyleSheet.create({
  wrap: {
    paddingTop: 20,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#F7F7F7'
  },
  search: {
    width: 40,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  nav: {
    marginLeft: 40,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  btn: {
    marginLeft: 10,
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0)'
  },
  btnTxt: {
    fontSize: 17
  }
});

export default CategoryNavs;