import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions
} from 'react-native'

const {width} = Dimensions.get('window');

const WideImg = ({source})=> {
  return (
      <View style={styles.wrap}>
        <Image onLayout={(evt)=> {
        }} source={source} resizeMode="cover" style={styles.wideImg}></Image>
      </View>

  )
};

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  wideImg: {
    width: width - 30,
    height: 200
  }
});

export default WideImg;