import React, {Component} from 'react';
import {
  View
} from 'react-native';

export default class Separate extends Component {
  render () {
    return (<View style={{
      height: this.props.height || 15,
      backgroundColor: '#F8F8F8'
    }}></View>);
  }
}