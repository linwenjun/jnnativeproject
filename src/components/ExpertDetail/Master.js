import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions
} from 'react-native';

const {width} = Dimensions.get('window');

const data = [{
  name: '精通行业',
  desc: '互联网+战略咨询，工业4.0与精益制造，物联网智慧城市，在线医疗互联网金融 ，物联网，智慧城市'
}, {
  name: '精通领域',
  desc: '互联网+战略咨询，工业4.0与精益制造，物联网'
}];

const Master = ()=> {
  return (
      <View style={styles.wrap}>
        {
            data.map(({name, desc}, key)=> (
                <View key={key} style={styles.master}>
                  <View  style={styles.name}>
                    <Text style={styles.nameText}>{name}</Text>
                  </View>
                  <View  style={styles.desc} >
                    <Text style={styles.descText} numberOfLines={2}>{desc}</Text>
                  </View>
                </View>
            ))
        }

      </View>


  )
};

const styles = StyleSheet.create({
  wrap: {
    alignItems: 'center',
    marginBottom: 20
  },
  master: {
    flexDirection: 'row',
    padding: 15,
    paddingTop: 5,
    paddingBottom: 5
  },
  name: {
    width: 66
  },
  nameText: {
    fontSize: 12,
    color: '#666'
  },
  descText: {
    fontSize: 12,
    color: '#31b2e3'
  },
  desc: {
    flex: 1
  }
});

export default Master;