import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native'

import WideImg from './WideImg';
import GrayLine from '../common/GrayLine';
import mainStyle from '../../common/mainStyle'

const About = ()=> {
  return (
      <View style={styles.wrap}>
        <View style={styles.title}>
          <Text style={styles.boldText}>关于专家</Text>
        </View>

        <WideImg source={require('../../../img/zb.png')}></WideImg>

        <Text style={[mainStyle.paragraph, styles.intro]}>
          马军，长安汽车股份有限公司，资深副总裁、首席专家。长安汽车自1984年进入汽车领域，2008年进入轿车领域，不断实现跨越式发展，截至2015年，研发实力行业排名第一，自主品牌销量行业第一，总体销量排名国内前三。
          自2002年以来，马军先生多年以来一直负责长安汽车集团公司管理创新、信息化建设及人力资源管理和领导工作，成功主导构建了先进的企业管理创新体系，管理创新成果多次荣获国家级奖项，成功实施长安汽车现代人力资源改革，构建人力资源领体系，打造了一支全球化的，国内领先的汽车人才队伍。
        </Text>
        <GrayLine />

        <View style={{
          marginTop: 15,
          marginBottom: 15
        }}>
          <Text style={styles.boldText}>曾任职的公司</Text>
        </View>

        <View style={{
          marginBottom: 15
        }}>
          <Text style={[mainStyle.paragraph]}>
            长安汽车股份有限公司 资深副总裁
          </Text>
          <Text style={[mainStyle.paragraph]}>
            爱德曼 企业品牌传播顾问
          </Text>
        </View>
        <GrayLine />

        <View style={{
          marginTop: 15
        }}>
          <Text style={styles.boldText}>项目经历</Text>
          <Text style={[styles.subTitle, {
            marginTop: 10,
            marginBottom: 10
          }]}>
            长安汽车ERP系统
          </Text>
          <Text style={[mainStyle.paragraph, {
            marginBottom: 15
          }]}>
            主导实施了长安汽车集团的多个大型信息化系统项目，包括集团ERP、OTD、HR系统、全分销系统DMS、工厂MES系统、公司采购一体化平台SRM以及长安全球协同研发系统PDM等实现了长安汽车对人、财、物，研发及产供销的一体化、集中式管理，同时完整地实现了以长安为核心、实时联结800多家供应商与经销商的供应链管理，主导长安汽车车联网、移动互联化等进程，助推形成了具有先进竞争水准的核心竞争力。
          </Text>
        </View>

        <GrayLine />

        <View style={{
          marginTop: 15
        }}>
          <Text style={[styles.subTitle, {
            marginTop: 10,
            marginBottom: 10
          }]}>
            长安汽车ERP系统
          </Text>
          <Text style={[mainStyle.paragraph, {
            marginBottom: 15
          }]}>
            主导实施了长安汽车集团的多个大型信息化系统项目，包括集团ERP、OTD、HR系统、全分销系统DMS、工厂MES系统、公司采购一体化平台SRM以及长安全球协同研发系统PDM等实现了长安汽车对人、财、物，研发及产供销的一体化、集中式管理，同时完整地实现了以长安为核心、实时联结800多家供应商与经销商的供应链管理，主导长安汽车车联网、移动互联化等进程，助推形成了具有先进竞争水准的核心竞争力。
          </Text>
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 15,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  title: {
    marginBottom: 15
  },
  intro: {
    marginTop: 15,
    marginBottom: 15
  },
  boldText: {
    fontWeight: 'bold'
  },
  subTitle: {
    fontSize:13
  }
});

export default About;
