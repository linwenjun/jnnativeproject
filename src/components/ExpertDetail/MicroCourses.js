import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import mainStyle from '../../common/mainStyle';
import WideImg from './WideImg';
import {
    ActivityStatistics
} from '../../public'

const MicroCourses = ()=> {
  return (
      <View style={styles.wrap}>
        <Text style={{
          marginBottom: 10,
          fontWeight: 'bold'
        }}>发布的微课</Text>
        <View>
          <WideImg source={require('../../../img/zb.png')}></WideImg>
          <Text style={[styles.subTitle, {
            marginTop: 10,
            marginBottom: 5
          }]}>工业4.0时代的企业治理如何破局？</Text>
          <Text style={[mainStyle.paragraph]}>
            十月份，锦囊微课堂特邀长安汽车副总裁马军先生为大家带来工业4.0专题课程。他将首次系统性总结长安汽车的转型创新之路。为正准备踏上制造业转型之路的企业、学员提供更多的经验和教训。为正准备踏上制造业转型之路的企业、学员提供更多的...
          </Text>
          <ActivityStatistics />
        </View>

      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    padding: 15
  },
  subTitle: {
    fontSize: 13
  }
});

export default MicroCourses;