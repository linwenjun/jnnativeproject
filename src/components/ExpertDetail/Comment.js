import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';

import mainStyle from '../../common/mainStyle';

const Comments = ()=> {
  return (
      <View style={styles.wrap}>
        <View style={styles.container}>
          <View>
            <Image source={require('../../../img/member_avatar.png')} style={styles.avatar} />
          </View>
          <View style={styles.content}>
            <View style={styles.time}>
              <Text style={styles.name}>钟广林</Text>
              <Image style={styles.icon} source={require('../../../img/time.png')}></Image>
              <Text style={styles.text}>2016.09.22</Text>
            </View>
            <View>
              <Text style={mainStyle.paragraph}>
                韩老师有丰富的创业经验，对精益创业有独到见解，给了很中肯的建议，受益匪浅。而且还给我介绍了业内朋友，真是非常感谢受益匪浅。而且还给我介绍了业内朋友，真是非常感谢！
              </Text>
            </View>
          </View>
        </View>
      </View>

  )
};

const styles = StyleSheet.create({
  wrap: {
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 15
  },
  container: {
    flexDirection: 'row'
  },
  avatar: {
    width: 50,
    height: 50
  },
  h1: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  icon: {
    width: 15,
    height: 15,
    marginRight: 6
  },
  text: {
    color: '#666',
    fontSize: 12
  },
  content: {
    flex: 1,
    marginLeft: 10
  },
  time: {
    flexDirection: 'row',
    marginBottom: 3
  },
  name: {
    fontSize: 13,
    marginRight: 10
  }
});

export default Comments;