import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import Comment from './Comment';
import GrayLine from '../common/GrayLine';
const Comments = ()=> {
  return (
      <View style={styles.wrap}>
        <Text style={styles.h1}>学员点评</Text>
        <View>
          <Comment />
          <GrayLine />
          <Comment />
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    padding: 15
  },
  h1: {
    fontSize: 14,
    fontWeight: 'bold'
  }
});

export default Comments;