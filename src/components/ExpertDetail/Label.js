import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native'

export default class Label extends Component {
  render() {
    return (
        <View>
          <View style={styles.labelContainer}>
            <Text style={styles.text}>{this.props.children}</Text>
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  labelContainer: {
    backgroundColor: '#32ace1',
    alignItems: 'center',
    padding: 0,
    height: 18,
    borderRadius: 9,
    borderTopRightRadius: 0,
    paddingLeft: 10,
    paddingRight: 10
  },
  text: {
    color: '#FFF',
    fontSize: 10,
    marginTop: 2
  }
});

