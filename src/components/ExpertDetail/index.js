import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions,
    ScrollView
} from 'react-native'

import ExpertAvatar from './ExpertAvatar';
import Separate from './Separate';
import Services from './Services';
import About from './About';
import MicroCourses from './MicroCourses';
import Comments from './Comments';

const ExpertDetail = ()=> {
  return (
      <View style={styles.wrap}>
        <ScrollView >
          <View style={styles.container}>
            <Image style={styles.bgImage} source={require('../../../img/expert_detail_head_background.png')}/>
            <ExpertAvatar style={styles.avatar}/>
            <Separate />
            <Services />
            <Separate />
            <About />
            <Separate />
            <MicroCourses />
            <Separate />
            <Comments />
          </View>
        </ScrollView>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1
  },
  container: {
    flexDirection: 'column'
  },
  bgImage: {
    padding: 0,
    height: 135,
    width: Dimensions.get('window').width,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  avatar: {
    marginTop: -43
  }
});

export default ExpertDetail;