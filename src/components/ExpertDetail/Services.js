import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native'

import ServiceIcon from '../../public/ServiceIcon'

const data = [
  {
    name: '工业4.0–标准化管理体系建设',
    price: '价格面议',
    type: 'advise'
  },
  {
    name: '工业4.0–标准化管理体系建设',
    price: '40/段',
    type: 'training'
  },
  {
    name: '工业4.0–标准化管理体系建设',
    price: '80/段',
    type: 'talk'
  }
];

const Services = ()=> {
  return (
      <View style={styles.wrap}>
        {
          data.map(({name, price, type}, idx, arr)=> {
            let isLast = (idx === arr.length - 1);
            return (
                <View key={idx} style={[styles.service, isLast ? {} : styles.serviceBottom]}>
                  <View style={styles.summary}>
                    <Text style={[styles.name, styles.text]}>{name}</Text>
                    <Text style={[styles.price, styles.text]}>{price}</Text>
                  </View>
                  <ServiceIcon size={40} type={type}/>
                </View>
            )
          })
        }
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    padding: 15,
    paddingTop: 0,
    paddingBottom: 0
  },
  service: {
    flexDirection: 'row',
    marginTop: 15,
    paddingBottom: 10
  },
  serviceBottom: {
    flex: 1,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1
  },
  summary: {
    flex: 1
  },
  text: {
    fontSize: 13
  },
  name: {
    color: '#333'
  },
  price: {
    marginTop: 8,
    color: '#F65131'
  }
});

export default Services;