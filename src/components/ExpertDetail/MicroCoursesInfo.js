import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';

const MicroCoursesInfo = ()=> {
  return (
      <View style={styles.wrap}>
        <View style={styles.info}>
          <Image style={styles.icon} source={require('../../../img/audience.png')}></Image>
          <Text style={styles.text}>1982</Text>
          <View style={styles.slug}></View>
          <Image style={styles.icon} source={require('../../../img/praise.png')}></Image>
          <Text style={styles.text}>143</Text>
        </View>
        <View style={styles.info}>
          <Image style={styles.icon} source={require('../../../img/time.png')}></Image>
          <Text style={styles.text}>2016.09.22</Text>
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    marginTop: 8,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  info: {
    flexDirection: 'row'
  },
  icon: {
    width: 15,
    height: 15,
    marginRight: 6
  },
  text: {
    color: '#666',
    fontSize: 12
  },
  slug: {
    width: 20
  }
});

export default MicroCoursesInfo;