import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native'

import Label from './Label';
import GrayLine from '../common/GrayLine';
import Master from './Master';
import RoundAvaterWithIcon from '../common/RoundAvatarWithIcon';

const ExpertAvatar = ({style})=> {
  return (
      <View style={[styles.wrap, style]}>
        <RoundAvaterWithIcon avatarImg={require('../../../img/expert_avatar.png')} />
        <Text style={styles.name}>李.斯文</Text>
        <Text style={styles.name}>艾曼德企业品牌传播顾问</Text>
        <View style={{
            marginTop: 18,
            marginBottom: 15,
            flexDirection: "row"
          }}>
          <Image resizeMode="contain" style={{
              height: 18,
              width: 100
            }} source={require('../../../img/Star.png')}></Image>
          <Label>1,234人访问过此专家</Label>
        </View>

        <GrayLine />

        <Text style={{
            margin: 15,
            fontSize: 12,
            lineHeight: 20,
            color: '#666'
          }} numberOfLines={10}>
          资深副总裁、首席专家，主导实施了长安汽车集团的多个大型信息化系统项目仲佳伟目前任佳伟拥有近7年公关及市场营销实务经验，在专注于品牌传播的同时，对产品故事及消费者行为充满兴趣及见解。
        </Text>

        <Master />

      </View>
  )
};


const styles = StyleSheet.create({
  wrap: {
    alignItems: 'center'
  },
  avatar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    height: 90,
    width: 90,
    borderRadius: 45,
    borderWidth: 2,
    borderColor: '#FFF'
  },
  fan: {
    height: 28,
    width: 28,
    borderRadius: 14,
    marginLeft: -24,
    marginTop: 60,
    borderWidth: 2,
    borderColor: '#FFF'
  },
  name: {
    marginTop: 6,
    color: '#666666'
  }
});

export default ExpertAvatar;