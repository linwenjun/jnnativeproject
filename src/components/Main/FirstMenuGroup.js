import React, { Component } from 'react';
import { TouchableWithoutFeedback, View, StyleSheet, Image, Text } from 'react-native';

const menuData1 = [{
  title: "互联网",
  imgSrc: require('../../../img/planet.png')
}, {
  title: "电商运营",
  imgSrc: require('../../../img/cart.png')
}, {
  title: "信息化",
  imgSrc: require('../../../img/technology.png')
}, {
  title: "品牌推广",
  imgSrc: require('../../../img/emoji.png')
}];

const menuData2 = [{
  title: "转型",
  imgSrc: require('../../../img/cloudy.png')
}, {
  title: "工业4.0",
  imgSrc: require('../../../img/factory.png')
}, {
  title: "媒体公关",
  imgSrc: require('../../../img/sofa.png')
}, {
  title: "其他",
  imgSrc: require('../../../img/trees.png')
}];


export default class FirstMenuGroup extends Component {
  render() {
    return (
        <View style={styles.wrap}>
          <View style={[styles.menuRow, styles.firstMenuRow]}>
            {
              menuData1.map((item, key)=> (
                  <TouchableWithoutFeedback key={key}  style={{flex: 1}}>
                    <View style={styles.menu}>
                      <Image style={styles.imageButton}
                             source={item.imgSrc}
                      ></Image>
                      <Text style={styles.title}>{item.title}</Text>
                    </View>
                  </TouchableWithoutFeedback>
              ))
            }
          </View>
          <View style={styles.menuRow}>
            {
              menuData2.map((item, key)=> (
                  <TouchableWithoutFeedback key={key}  style={{flex: 1}}>
                    <View style={styles.menu}>
                      <Image style={styles.imageButton}
                             source={item.imgSrc}
                      ></Image>
                      <Text style={styles.title}>{item.title}</Text>
                    </View>
                  </TouchableWithoutFeedback>
              ))
            }
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  wrap: {
  },
  title: {
    color: '#4C4C4C',
    marginTop: 10,
    fontSize: 12
  },
  menuRow: {
    flexDirection: "row",
    backgroundColor: '#FFF'
  },
  firstMenuRow: {
    borderTopWidth: 1,
    borderTopColor: '#D8D8D8',
  },
  menu: {
    flex: 1,
    height: 90,
    borderRightWidth: 1,
    borderRightColor: '#D8D8D8',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    alignItems: 'center'
  },
  imageButton: {
    width: 50,
    height: 50,
    marginTop: 4
  }
});