import React, {Component} from 'react';
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TextInput,
    Dimensions
} from 'react-native'

import SearchBar from './SearchBar';
import MainCarousel from './MainCarousel';
import MiddleMenus from './MiddleMenus';
import ProjectList from './ProjectList';
import ExpertList from './ExpertList';
import NavMenus from '../../public/NavMenus';

import {
    createRouter,
    NavigationProvider,
    StackNavigation
} from '@exponent/ex-navigation';

const Router = createRouter(()=> ({
  "main": ()=> MainScene
}));

componentList = [
  SearchBar,
  MainCarousel,
  MiddleMenus,
  ProjectList,
  ExpertList
];

class MainScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      children: []
    }
  }

  loadComponent() {
    requestAnimationFrame(()=> {
      let children = this.state.children;
      let len = children.length;
      if (len < componentList.length) {
        let SubComponent = componentList[len];
        children.push(<SubComponent key={len}/>);
        this.setState({
          children
        });
        this.loadComponent()
      }
    });
  }

  componentDidMount() {
    this.loadComponent();
  }

  render() {
    return (
        <View style={styles.container}>
          <ScrollView ref={(ref)=> {
            this.scrollView = ref;
          }} style={styles.container}>
            <SearchBar />
            <MainCarousel />
            <MiddleMenus />
            <ProjectList />
            <ExpertList />
          </ScrollView>
          <NavMenus />
        </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default class Main extends Component {
  render() {
    return (
        <NavigationProvider router={Router}>
          <StackNavigation initialRoute={Router.getRoute('main')}/>
        </NavigationProvider>
    )
  }
}