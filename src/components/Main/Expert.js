import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    TouchableOpacity,
    NativeModules
} from 'react-native';

import ExpertDetail from '../ExpertDetail';
import NullComponent from '../NullComponent/index';

import { withNavigation, createRouter } from '@exponent/ex-navigation';

const Router = createRouter(() => ({
  detail: () => ExpertDetail
}));

@withNavigation
export default class Expert extends Component {
  render() {
    const {imgSrc, name, title, domain} = this.props.data
    return (
        <TouchableOpacity style={styles.expert} onPress={()=> {
          // this.props.navigator.push(Router.getRoute('detail'));
          const {StoreModule} = NativeModules;
          StoreModule.goActivity("aaaaa");
        }}>
          <View style={styles.expert}>
            <Image style={styles.img} source={imgSrc}></Image>
            <View style={styles.nameWrap}>
              <Text style={styles.name}>{name}</Text>
              <Image resizeMode="contain" style={styles.star} source={require('../../../img/Star.png')}></Image>
            </View>
            <Text style={styles.title}>{title}</Text>
            <Text numberOfLines={2} style={styles.domain}>{domain}</Text>
          </View>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  expert: {
    marginTop: 10,
    width: Dimensions.get('window').width / 2 - 15,
    height: 320
  },
  img: {
    width: Dimensions.get('window').width / 2 - 15,
    height: 222,
    borderRadius: 3
  },
  nameWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  name: {
    marginRight: 10,
    fontSize: 13,
    fontWeight: 'bold'
  },
  star: {
    width: 70,
    height: 20
  },
  title: {
    marginTop: 10,
    fontSize: 13
  },
  domain: {
    marginBottom: 10,
    marginTop: 10,
    fontSize: 11,
    color: "#33ADE2"
  }

});