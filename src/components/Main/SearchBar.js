import React, {Component} from 'react';
import {View, Text, Image, TextInput, StyleSheet} from 'react-native';

export default class SearchBar extends Component {
  render() {
    return (
        <View style={[styles.header, this.props.style]}>
          <View style={styles.searchContainer}>
            <View style={styles.search}>
              <Image style={styles.searchIcon}
                     source={require('../../../img/Search.png')}
              ></Image>
              <TextInput
                  underlineColorAndroid="transparent"
                  style={styles.searchInput}></TextInput>
            </View>
            <Image style={styles.message}
                  source={require('../../../img/Box.png')}>
            </Image>
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#EC4F2F",
    justifyContent: "flex-end"
  },
  searchContainer: {
    marginTop: 11,
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center'
  },
  search: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: "#FFF",
    borderRadius: 3,
    height: 36,
    alignItems: 'center'
  },
  searchIcon: {
    width: 20,
    height: 20,
    marginLeft: 5,
    marginRight: 4
  },
  searchInput: {
    flex: 1,
    padding: 0
  },
  message: {
    width: 20,
    height: 20,
    marginLeft: 15,
    marginRight: 7
  }
});