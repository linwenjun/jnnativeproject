import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, PixelRatio, Dimensions } from 'react-native';


export default class Project extends Component {
  render() {
    const {imgSrc, title, category} = this.props.data;
    return (
        <View style={styles.wrap}>
          <Image resizeMode="cover" style={styles.img} source={imgSrc}></Image>
          <View style={styles.titleWrap}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <Text style={styles.category}>{category}</Text>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  wrap: {
    margin: 10,
    borderRadius: 3,
    overflow: 'hidden'

  },
  img: {
    width: Dimensions.get('window').width - 20,
    height: 215,
    overflow: 'hidden',
    borderRadius: 3
  },
  titleWrap: {
    borderBottomWidth: 1 / PixelRatio.get(),
    borderBottomColor: '#000'
  },
  title: {
    marginTop: 16,
    fontSize: 14,
    paddingBottom: 16,
    fontWeight: 'bold'
  },
  category: {
    marginTop: 12,
    fontSize: 12,
    color: "#00AFE2"
  }
});
