import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import Project from './Project';

const projects = [{
  imgSrc: require('../../../img/subject.png'),
  title: 'CMO怎样才能找到合适的市场人员',
  category: '电商，互联网，线上到线下'
}, {
  imgSrc: require('../../../img/subject.png'),
  title: 'CMO怎样才能找到合适的市场人员',
  category: '电商，互联网，线上到线下'
}, {
  imgSrc: require('../../../img/subject.png'),
  title: 'CMO怎样才能找到合适的市场人员',
  category: '电商，互联网，线上到线下'
}];

export default class ProjectList extends Component {
  render() {
    return (
        <View>
          {
              projects.map((item, key)=> (
                  <Project data={item} key={key}></Project>
              ))
          }
        </View>
    )
  }
}