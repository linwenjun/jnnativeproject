import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';

import Expert from './Expert';

const experts = [{
  imgSrc: require('../../../img/expert.png'),
  title: '资深娱乐',
  name: '刘玉清',
  domain: '电商，互联网，线上到线下, 电商，互联网，线上到线下'
}, {
  imgSrc: require('../../../img/expert.png'),
  title: '资深娱乐作家',
  name: '刘玉清',
  domain: '电商，互联网，线上到线下'
}, {
  imgSrc: require('../../../img/expert.png'),
  title: '资深娱乐作家',
  name: '刘玉清',
  domain: '电商，互联网，线上到线下'
}, {
  imgSrc: require('../../../img/expert.png'),
  title: '资深娱乐作家',
  name: '刘玉清',
  domain: '电商，互联网，线上到线下'
}];

export default class ExpertList extends Component {
  render() {
    return (
        <View style={styles.wrap}>
          {
            experts.map((item, key)=> (
              <Expert data={item} key={key}></Expert>
            ))
          }
        </View>
    )
  }
}

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: 10
  },
  expert: {
    marginTop: 10,
    width: Dimensions.get('window').width / 2 - 15,
    height: 470
  },
  img: {
    width: Dimensions.get('window').width / 2 - 15,
    height: 350,
    borderRadius: 3
  },
  nameWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  name: {
    marginRight: 10,
    fontSize: 16,
    fontWeight: 'bold'
  },
  star: {
    width: 90,
    height: 20
  },
  title: {
    marginTop: 10,
    fontSize: 16
  },
  domain: {
    marginBottom: 10,
    marginTop: 10,
    color: "#33ADE2"
  }

});