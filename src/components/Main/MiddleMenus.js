import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';

import FirstMenuGroup from './FirstMenuGroup';

export default class MiddleMenus extends Component {
  render() {
    return (
        <View  style={styles.wrapper}>
          <FirstMenuGroup />
        </View>
    )
  }
}

var styles = StyleSheet.create({
  wrapper: {
    overflow: 'hidden'
  },
  paginationStyle: {
    top: 0
  }
});