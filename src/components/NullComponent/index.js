import React, { Component } from 'react';
import { View, Text } from 'react-native'
import { withNavigation } from '@exponent/ex-navigation';

@withNavigation
export default class NullComponent extends Component{
  static route = {
    navigationBar: {
      title: 'null'
    }
  }

  render() {
    return (
        <View style={{
          flex: 1,
          backgroundColor: '#FFF',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <Text style={{
            fontSize: 30,
            fontWeight: 'bold'
          }} onPress={()=> {
            this.props.navigator.pop();
          }}>Goodbye, World!</Text>
        </View>
    )
  }
}