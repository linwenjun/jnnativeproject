import React, {Component} from 'react';
import {
    View,
    Dimensions
} from 'react-native'

const GrayLine = ({style})=> {
  return (
      <View style={[{
            borderBottomWidth: 1,
            borderBottomColor: '#EBEBEB',
            height: 1
          }, style]}/>
  )
};

export default GrayLine;
