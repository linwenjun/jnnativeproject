import React, {Component} from 'react';
import {
    View,
    Text,
  StyleSheet
} from 'react-native';

import {
  Space,
    ActivityStatistics
} from '../../public';

const Summary = ()=> {
  return (
      <View style={styles.wrap}>
        <Text style={styles.title}>用游戏化改变世界</Text>
        <Space height={6} />
        <ActivityStatistics/>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: '#FFF',
    padding: 15
  },
  title: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  data: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  left: {
    flexDirection: 'row'
  }
});

export default Summary;