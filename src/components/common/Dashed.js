import React, {Compoent} from 'react';
import {
    Image,
    StyleSheet
} from 'react-native';

const Dashed = ()=> {
  return (
      <Image style={styles.wrap} resizeMode="repeat" source={require('../../../img/dashed.png')}></Image>
  )
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    height: 5,
    width: null
  }
});

export default Dashed;