import React, {Compoent} from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import {
    GrayLine,
    Space
} from '../../public';

const ActivityIntro = ()=> {
  return (
      <View>
        <Text style={styles.title}>活动介绍</Text>
        <Space />
        <GrayLine />
        <Space />
        <Text style={styles.content}>
          如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，者觉得在第一种情况基础……如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，者觉得在第一种情况基础……如果你们家的店搞不定第一种情况，或者搞不好第一种情况，或者觉得在第一种情况基础上还想再做做第二种情况的文这是我最近在做的事情，者觉得在第一种情况基础……
        </Text>
        <Space />
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: '#FFF'
  },
  title: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  content: {
    fontSize: 12,
    lineHeight: 18,
    color: 'rgba(0,0,0,0.6)'
  }
});

export default ActivityIntro;