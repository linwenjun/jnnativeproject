import React, {Compoent} from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import {
  Space,
  GrayLine
} from '../../public';

const SubTitle = (props)=> {
  return (
      <View>
        <Text style={styles.text}>{props.children}</Text>
        <Space />
        <GrayLine />
      </View>
  )
};

const styles = StyleSheet.create({
  text: {
    fontSize: 13,
    fontWeight: "bold"
  }
});

export default SubTitle;