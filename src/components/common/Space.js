import React, {Component} from 'react';
import {
  View
} from 'react-native';

const Space = (props)=> {
  return (
      <View style={{
        height: props.height || 15,
        backgroundColor: props.color || 'rgba(0,0,0,0)'
      }}/>
  )
};

export default Space;