import React, {Compoent} from 'react';
import {
  View,
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const white = '#FFF';

const TopBar = ()=> {
  return (
      <View style={styles.wrap}>
        <Icon style={{
            marginLeft: 10
          }} name="chevron-left"  size={18} color={white}/>
      </View>
  )
};

const styles = StyleSheet.create({
    wrap: {}
});

export default TopBar;