import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet
} from 'react-native';

const iconImg = require('../../../img/fan.png');

const RoundAvatarWithIcon = ({avatarImg, size=90, style})=> {
  return (
      <View style={[styles.avatar, style]}>
        <Image style={[styles.image, {
            height: size,
            width: size,
            borderRadius: size / 2

        }]}
               source={avatarImg}></Image>
        <Image style={[styles.fan, {
            height: size * 0.3,
            width: size * 0.3,
            borderRadius: size * 0.15,
            marginLeft: - size * 0.3,
            marginTop: size * 2 / 3,
        }]}
               source={iconImg}></Image>
      </View>
  );
};

const styles = StyleSheet.create({
  avatar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    borderWidth: 2,
    borderColor: '#FFF'
  },
  fan: {
    borderWidth: 2,
    borderColor: '#FFF'
  }
});

export default RoundAvatarWithIcon;