import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image
} from 'react-native';

import RoundAvatarWithIcon from './RoundAvatarWithIcon';
import GrayLine from './GrayLine';
import FiveStarIcon from '../../public/FiveStarIcon';

const ExpertItem = ({style})=> {
  let expertImg = require('../../../img/expert_avatar.png');
  return (
      <View style={[styles.wrap, style]} >
        <View style={styles.expertItem}>
          <RoundAvatarWithIcon style={{
                                marginRight: 14
                              }}
                               size={81}
                               avatarImg={expertImg}/>
          <View style={styles.summary}>
            <View style={styles.nameWrap}>
              <Text style={styles.name}>周郁凯</Text>
              <FiveStarIcon size={17} />
            </View>
            <Text style={styles.title}>知名国际行为分析学者</Text>
            <Text style={styles.domain}>公司管理，市场，招聘，互联网，实体店营销</Text>
            <Text
                style={styles.desc}>新旗互动创始人&首席病毒官，前WeMedia新媒体集团创意总监，病毒营销第一人，网络红人@校门舞男。新旗互动获得16届IAI国际广告奖病毒营销类金奖并斩获全场大奖。</Text>
          </View>
        </View>
        <GrayLine style={{
          marginTop: 10
        }} />
      </View>

  )
};

const styles = StyleSheet.create({
  wrap: {
    marginTop: 10
  },
  expertItem: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  summary: {
    flex: 1,
    flexDirection: 'column'
  },
  nameWrap: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  name: {
    fontSize: 13,
    fontWeight: 'bold',
    marginRight: 8
  },
  title: {
    fontSize: 13,
    marginTop: 9
  },
  domain: {
    fontSize: 11,
    color: '#46B4E4',
    marginTop: 9
  },
  desc: {
    fontSize: 11,
    marginTop: 10
  }
});

export default ExpertItem;