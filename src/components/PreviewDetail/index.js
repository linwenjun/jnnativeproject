import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    ScrollView,
    Dimensions
} from 'react-native';

import {
    Space,
    GrayLine,
    ExpertItem
} from '../../public';

import {
    Summary,
    ActivityIntro,
    SubTitle
} from '../common';

import SignUp from './SignUp';

let wWidth = Dimensions.get('window').width;

const PreviewDetail = ()=> {
  return (
      <ScrollView style={styles.wrap}>
        <View style={styles.imageWrap}>
          <Image resizeMode="cover" style={styles.image} source={require('../../../img/activity_background.png')}/>
        </View>
        <Space />
        <Summary />

        <View style={styles.section}>
          <ActivityIntro />
        </View>

        <Space />

        <View style={styles.section}>
          <SubTitle>讲师</SubTitle>
          <ExpertItem />
        </View>

        <Space />

        <View style={styles.section}>
          <SubTitle>报名方式</SubTitle>
          <Space />
          <SignUp />
        </View>


      </ScrollView>

  )
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: '#F8F8F8'
  },
  imageWrap: {
    flexDirection: 'column'
  },
  image: {
    width: wWidth,
    height: 290
  },
  section: {
    padding: 15,
    backgroundColor: '#FFF'
  }
});

export default PreviewDetail;