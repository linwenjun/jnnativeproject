import React, {Compoent} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native';

import {
  Dashed
} from '../common'

const QRCode = ()=> {
  return (
      <View style={styles.wrap}>
        <Image resizeMode="contain" style={styles.image} source={require('../../../img/qr_code.png')}/>
        <View style={styles.textWrap}>
          <Dashed />
          <Text style={styles.text}>
            小锦微信的二维码
          </Text>
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    borderWidth: 1,
    borderColor: '#d1d1d1'
  },
  image: {
    height: 140,
    width: 140
  },
  textWrap: {
    flexDirection: 'column'
  },
  text: {
    textAlign: 'center',
    fontSize: 13,
    lineHeight: 28,
    color: '#848484'
  }
});

export default QRCode;