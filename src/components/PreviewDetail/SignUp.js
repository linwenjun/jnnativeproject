import React, {Compoent} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';

import {
  Dashed,
  Space
} from '../common';

import QRCode from './QRCode';

const SingUp = ()=> {
  return (
      <View>
        <View style={styles.methods}>
          <QRCode />
          <Image resizeMode="contain" style={styles.or} source={require('../../../img/or.png')}></Image>
          <QRCode />
        </View>
        <Space height={20}/>
        <Dashed />
        <View style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: -10
        }}>
          <Text style={styles.group}>添加小锦微信,小锦将您拉入群</Text>
        </View>
        <Space />

      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {},
  methods: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  or: {
    width: 40
  },
  group: {
    backgroundColor: '#FFF',
    paddingLeft: 5,
    paddingRight: 5,
    color: '#848484'
  }
});

export default SingUp;