import React from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet
} from 'react-native';


import ServiceIcon from '../../public/ServiceIcon';
import GrayLine from '../common/GrayLine';

const mockDescList = [{
  title: '内容描述',
  content: '2015年，“互联网+”写入李克强总理的政府工作报告，这意味着“互联网+”正式被纳入顶层设计，成为国家经济社会发展的重要战略。'
}, {
  title: '适合对象',
  content: '传统企业的董事长、总经理、企业中高管。'
}];

const ServiceDetail = ({price='面议'})=> {
  return (
      <View style={styles.wrap}>
        <ScrollView style={styles.scroll}>
          <View style={styles.title}>
            <ServiceIcon size={44}/>
            <View style={styles.content}>
              <Text style={styles.contentText}>
                互联网时代的传统企业转型探索与实践互联网时代的传统企业转型探索与实践
              </Text>
            </View>
          </View>
          
          <GrayLine style={{
            marginTop: 15
          }}/>

          {
            mockDescList.map(({title, content}, idx)=> {
              return (
                  <View key={idx} style={styles.desc}>
                    <Text style={styles.descTitle}>{title}</Text>
                    <Text style={styles.descContent}>{content}</Text>
                  </View>
              )
            })
          }
        </ScrollView>

        <View style={styles.book}>
          <View style={styles.price}>
            <Text style={styles.priceText}>{price}</Text>
          </View>
          <View style={styles.bookBtn}>
            <Text style={styles.bookBtnText}>立即预约</Text>
          </View>
        </View>
      </View>

  )
};



const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 20
  },
  scroll: {
    paddingLeft: 15,
    paddingRight: 15
  },
  title: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10
  },
  contentText: {
    lineHeight: 20,
    color: '#1F1F1F'
  },
  desc: {
    marginTop: 10,
    marginBottom: 6
  },
  descTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333'
  },
  descContent: {
    marginTop: 8,
    fontSize: 13,
    color: '#333',
    lineHeight: 20
  },
  book: {
    height: 50,
    flexDirection: 'row'
  },
  price: {
    borderTopWidth: 1,
    borderTopColor: '#E6E6E6',
    width: 108,
    alignItems: 'center',
    justifyContent: 'center'
  },
  priceText: {
    color: '#F65131',
    fontSize: 16
  },
  bookBtn: {
    borderTopWidth: 1,
    borderTopColor: '#F65131',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F65131'
  },
  bookBtnText: {
    color: '#FFF',
    fontSize: 16
  }
});

export default ServiceDetail