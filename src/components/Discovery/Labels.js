import React, {Compoent} from 'react';
import {
    Text,
    StyleSheet
} from 'react-native';

const Labels = ({children})=> {
  return (
      <Text style={styles.wrap}>{children}</Text>
  )
};

const styles = StyleSheet.create({
  wrap: {
    color: '#28ABE3',
    fontSize: 11
  }
});

export default Labels;