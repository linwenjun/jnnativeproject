import React, {Compoent} from 'react';
import {
    Text,
    View,
    StyleSheet
} from 'react-native';

import {
    Space,
    GrayLine
} from '../common'

const Title = ({children})=> {
  return (
      <View>
        <View style={styles.container}>
          <Text style={styles.text}>{children}</Text>
          <Text style={styles.more}>更多</Text>
        </View>
        <Space height={9} />
        <GrayLine />
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {},
  text: {
    fontSize: 15
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  more: {
    color: '#F65131'
  }
});

export default Title;