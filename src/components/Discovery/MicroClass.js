import React, {Compoent} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native';

import {
    ActivityStatistics2,
    Space
} from '../common';

const MicroClass = ()=> {
  let imgSrc = require('../../../img/class.png')
  return (
      <View style={styles.wrap}>
        <Image resizeMode='contain' style={styles.img} source={imgSrc}></Image>
        <View style={styles.info}>
          <View><Text style={styles.title} numberOfLines={1}>如何用低成本做成更大的媒体影视如何用低成本做成更大的媒体影视</Text></View>
          <View style={styles.row}>
            <Text style={styles.name}>李星</Text>
            <Text style={styles.job}>知名策划人</Text>
          </View>
          <ActivityStatistics2 />
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  img: {
    flex: 1,
    width: null,
    height: 80
  },
  info: {
    flex: 2,
    marginLeft: 10,
    justifyContent: 'space-between'
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold'
  },

  name: {
    color: '#3A3A3A'
  },
  job: {
    marginLeft: 14,
    color: '#666',
    fontSize: 12
  }
});

export default MicroClass;