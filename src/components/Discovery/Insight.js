import React, {Compoent} from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import Labels from './Labels';
import {Space, RoundAvatarWithIcon, ActivityStatistics2} from '../common';

const Insight = ()=> {
  return (
      <View>
        <Labels>金融,线上到线下,互联网</Labels>
        <Space height={7}/>
        <Text style={styles.question}>创业公司，怎么做市场推广？品牌价值提升？在市场很快有声音？</Text>
        <Space height={20}/>
        <View style={styles.avatar}>
          <RoundAvatarWithIcon size={45} avatarImg={require('../../../img/expert_2.png')}/>
          <View style={styles.info}>
            <Text style={styles.name}>李星</Text>
            <Text style={styles.job}>一系集团控股董事长</Text>
          </View>
        </View>

        <Space />
        <Text style={styles.answer}>
          主要是品牌公关最佳需要可以联系我，族谱科技帮你解决问题。主要是品牌公关最佳需要可以联系我，族谱科技帮你解决问题。主要是品牌公关最佳需要可以联系我，族谱科技帮你解决问题。主要是品牌公关最佳需要可以联系我，族...
        </Text>

        <Space />

        <View style={styles.half}>
          <ActivityStatistics2 />
        </View>

      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {},
  question: {
    fontWeight: 'bold',
    lineHeight: 20
  },
  avatar: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  info: {
    marginLeft: 10
  },
  name: {
    marginTop: 8
  },
  job: {
    marginTop: 6,
    fontSize: 11
  },
  answer: {
    fontSize: 13,
    color: '#333',
    lineHeight: 18
  },
  half: {
    width: 160
  }
});

export default Insight;