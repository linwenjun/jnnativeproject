import React, {Compoent} from 'react';
import {
    View,
    ScrollView,
    StyleSheet
} from 'react-native';

import {NavMenus} from '../../public';
import {Space, GrayLine} from '../common';
import Title from './Title';
import MicroClass from './MicroClass';
import Insight from './Insight';

const Discovery = ()=> {
  return (
      <View style={styles.wrap}>
        <ScrollView style={styles.wrap}>
          <View style={styles.section}>
            <Title>微课</Title>
            <Space />

            <MicroClass />
            <GrayLine style={{
              marginTop: 10,
              marginBottom: 10
            }}></GrayLine>
            <MicroClass />

          </View>

          <Space />

          <View style={styles.section}>
            <Title>献计</Title>
            <Space />
            <Insight />
            <GrayLine style={{
              marginTop: 10,
              marginBottom: 10
            }}></GrayLine>
            <Insight />
          </View>

        </ScrollView>
        <NavMenus />
      </View>
  )
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: '#F8F8F8'
  },
  section: {
    backgroundColor: '#FFF',
    padding: 15
  }
});

export default Discovery;