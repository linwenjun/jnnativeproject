import { Provider } from 'react-redux';
import { createStore } from 'redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import App from './src/App';
import rootReducers from './src/reducers';

const store = createStore(rootReducers);

export default class JNProject extends Component {
  render() {
    return (
        <Provider store={store}>
          <App></App>
        </Provider>
    );
  }
}

AppRegistry.registerComponent('JNProject', () => JNProject);
