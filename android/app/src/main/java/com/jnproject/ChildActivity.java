package com.jnproject;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by wjlin on 11/27/16.
 */
public class ChildActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child);
    }
}
