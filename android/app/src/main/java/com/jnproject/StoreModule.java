package com.jnproject;

import android.content.Intent;
import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by wjlin on 11/27/16.
 */
public class StoreModule extends ReactContextBaseJavaModule {
    public StoreModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "StoreModule";
    }

    @ReactMethod
    public void goActivity(String name) {
        Toast.makeText(getReactApplicationContext(), name+"", Toast.LENGTH_SHORT).show();
        getCurrentActivity().startActivity(new Intent(getCurrentActivity(), ChildActivity.class));
    }
}

